/*

Scrivere un programma per la gestione di un autosalone che memorizzi per ogni auto modello, targa, colore, prezzo.
Definire le strutture dati necessarie a memorizzare una automobile e dichiarare una variabile per memorizzare al massimo 100 auto.
Chiedere all'utente di inserire i dati di n auto (con n definito dall'utente e minore o uguale di 100). Dopo l'inserimento permettere all'utente di
cercare un'auto tramite la targa

*/

#include <stdio.h>
#include <string.h>

#define N 100

typedef struct {
    char modello[1000];
    char targa[8]; //le targhe italiane hanno 7 caratteri. Metto 8 perchè c'è il carattere terminatore
    char colore[100];
    float prezzo;
} automobile;


int leggiNumeroInteroPositivo();

int main()
{
    automobile a[N];
    char targa[8];

    int i, n = leggiNumeroInteroPositivo(), trovata = 0;

    for (i=0; i<n; i++) {
        printf("Inserire modello: ");
        scanf("%[^\n]", a[i].modello);

        printf("Inserire targa: ");
        scanf("%s%*c", a[i].targa);

        printf("Inserire colore: ");
        scanf("%[^\n]", a[i].colore);

        printf("Inserire prezzo: ");
        scanf("%f%*c", &a[i].prezzo);
    }

    printf("Inserire la targa dell'auto da cercare: ");
    scanf("%s%*c", targa);

    for (i=0; i<n && trovata==0; i++) {
        if(strcmp(a[i].targa,targa) == 0) {
            trovata = 1;
            printf("*** AUTO TROVATA ***\n");
            printf("Modello: %s\n", a[i].modello);
            printf("Targa: %s\n", a[i].targa);
            printf("Colore: %s\n", a[i].colore);
            printf("Prezzo: %f\n", a[i].prezzo);
        }
    }

    if(trovata == 0) {
        printf("AUTO NON TROVATA\n");
    }


    return 0;
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Insersci la lunghezza: ");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig  || num > N); //cicla finchè non inseriamo un numero positivo e interno, e minore di N poichè la lunghezza massima della stringa è N

    return num;
}
