/*

HO FATTO SOLO LO STOP 5, mancano gli step precedenti (presenti nel laboratorio 5) e quelli successivi

*/

#include <stdio.h>
#include <stdlib.h> // srand, rand
#include <time.h>// time

struct stats
{
    float x;
    float y;
    int health;
    int strength;
};

typedef struct stats *monster;
typedef struct stats *player;

#define maxHealth 10000
#define maxStrength 800

monster create_monster(int max_health, int max_strength);
void combat(player p, monster m);

int main()
{
    srand(time(NULL));

//CREO E POPOLO LE STATS DEL MOSTRO
    monster m = NULL;
    m = create_monster(maxHealth, maxStrength);

//POPOLO LE STATS DEL PLAYER
    player p = malloc(sizeof(struct stats));
    p->health = rand()%(maxHealth+1) + 1;//genera un numero fra 1 e maxHealth
    p->strength = rand()%(maxStrength+1) + 1; //genera un numero fra 1 e maxStrength

//INIZIA CICLO COMBATTIMENTO FINCHE' UNO DEI DUE MUORE
    while(m->health > 0 && p->health > 0)
        combat(p, m);

//STAMPO IL RISULTATO DEL COMBATTIMENTO
    if(m->health < 0) {
        printf("Il mostro è morto e ha vinto il giocatore, a cui rimangono %d punti vitali", p->health);
    }
    else if(p->health < 0) {
        printf("Il player è morto e ha vinto il mostro, a cui rimangono %d punti vitali", m->health);
    }
    else {
        printf("QUALCOSA NON VA");
    }

    return 0;
}

monster create_monster(int max_health, int max_strength) {

    int health, strength;

    health = rand()%(max_health+1) + 1; //genera un numero fra 1 e max_health
    strength = rand()%(max_strength+1) + 1; //genera un numero fra 1 e max_strength

    monster enemy = malloc(sizeof(struct stats));
    enemy->health = health;
    enemy->strength = strength;

    return enemy;

}

void combat(player p, monster m) {

    int first = rand()%2; //genera un numero fra 0 e 1
    int attackP, attackM;

    attackP = rand()%(p->strength + 1); //genera l'attacco del player come numero compreso tra 0 e la strength del player
    attackM = rand()%(m->strength + 1); //genera l'attacco del monster come numero compreso tra 0 e la strength del monster

//ATTACCA PRIMA IL GIOCATORE
    if(first == 0) {
        m->health-=attackP; //tolgo alla vita del monster il valore dell'attacco del player

        if(m->health > 0) { //se il mostro sopravvive
            p->health-=attackM; //tolgo alla vita del player il valore dell'attacco del monster
        }
    }
// ATTACCA PRIMA IL MOSTRO
    else {
        p->health-=attackM; //tolgo alla vita del monster il valore dell'attacco del player

        if(p->health > 0) { //se il mostro sopravvive
            m->health-=attackP; //tolgo alla vita del player il valore dell'attacco del monster
        }
    }
}
