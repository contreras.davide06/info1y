//

#include <stdio.h>

int main() {
    
    int i;

    int array [10] = {0,1,2,3,4,5,6,7,8,9};
    
    for (i=0; i<10; i++){
        
        printf("array[%d]: %d\n", i, array[i]);
    
    }
    
    printf("\n\nREVERSE ARRAY\n\n");
    
    for (i=9; i>=0; i--) {
        
        printf("array [%d]: %d\n",i,array[i]);
        
    }
    
    printf("\n\n");
            
    printf("Inserire indice array: ");
    
    scanf("%d", &i);
        
    printf("array[%d]: %d\n",i, array[i]);


    return 0;

}
