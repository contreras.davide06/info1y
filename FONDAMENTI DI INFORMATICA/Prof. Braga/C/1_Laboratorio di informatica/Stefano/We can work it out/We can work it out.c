#include <stdio.h>
#include <string.h>
#include <math.h>

void alphaorder(FILE *file);
int titoli(FILE *file);

int titoli(FILE *file);

int main(){
	
	FILE *file;
	if((file=fopen("canzoniere.txt", "r"))!=NULL){
		titoli(file);
		fclose(file);
	}
	else{
		printf("Errore nell'apertura del programma");
	}
	
	return 0;
}

int titoli(FILE *file){	//restituisce anche il numero di titoli trovati
	FILE *filedest;
	int d=0, riga=0, linea=0;
	char str[100]="";
	for(; !feof(file); ){
		fgets(str, 100, file);
		riga++;
		str[strlen(str)-1]='\0';
		if((strcmp(str, "**********"))==0){
			d++;
			fgets(str, 100, file);
			riga++;
			str[strlen(str) -1]='\0';
			if((filedest=fopen("titoli-linea-num.linee.txt", "a"))!=NULL){
				if(d!=1){
					fprintf(filedest, "[Num. linee: %d]\n", riga-linea-4);
				}
				fprintf(filedest, "%d) %s [inizia alla linea: %d] ", d, str, riga);
				linea=riga;
				fclose(filedest);
			}
			else{
				printf("Errore nella scrittura di \"titoli.txt\"");
			}
		}
	}
	if((filedest=fopen("titoli.txt", "a"))!=NULL){
		fprintf(filedest, "[Num. linee: %d]\n", riga-linea-4);
		fclose(filedest);
	}
	else{
		printf("Errore nella scrittura di \"titoli.txt\"");
	}
	return d;
}
