#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


typedef char TipoElemento;

typedef struct EL{
	TipoElemento info;
	struct EL * prox;
} ElemLista;

typedef ElemLista * ListaDiElem;

ListaDiElem insinordineric(ListaDiElem lista, TipoElemento elem);
int verificapresenzaric(ListaDiElem lista, TipoElemento elem);
void visualizzalistaric(ListaDiElem lista);
void distruggilistaric(ListaDiElem lista);
ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem);
ListaDiElem invertilista(ListaDiElem lista);
ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem);
ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem);
int contaelem(ListaDiElem lista);

int main(){
	
	int n;
	ListaDiElem lista;
	lista=NULL;
	for(n=6; n<=11; n++){
		lista=insintesta(lista, n);
	}
	lista=insinordineric(lista, 6);
	visualizzalistaric(lista);
	lista=cancellaunoric(lista, 11);
	visualizzalistaric(lista);
	if(verificapresenzaric(lista, 10)){
		printf("\nE' contenuto\n");
	}
	else{
		printf("\nNon e' contenuto\n");
	}
	distruggilistaric(lista);
	lista=NULL;
	visualizzalistaric(lista);
	return 0;
}

ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista==NULL){
		return lista;
	}
	if(lista->info!=elem){
		lista->prox=cancellaunoric(lista->prox, elem);
		return lista;
	}
	else if(lista->info==elem){
		temp=lista->prox;
		free(lista);
		return temp;
	}
}

ListaDiElem insinordineric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista==NULL){
		temp=malloc(sizeof(ElemLista));
		temp->info=elem;
		temp->prox=NULL;
		return temp;
	}
	if(lista->info<elem){
		temp=malloc(sizeof(ElemLista));
		temp->info=elem;
		temp->prox=lista;
		return temp;
	}
	if(lista->info>elem){
		lista->prox=insinordineric(lista->prox, elem);
	}
	return lista;
}

ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem){
	if(lista==NULL){
		lista=malloc(sizeof(ElemLista));
		lista->info=elem;
		lista->prox=NULL;
		return lista;
	}
	else{
		lista->prox=insincodaric(lista->prox, elem);
	}
	return lista;
}

ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	temp=malloc(sizeof(ElemLista));
	temp->info=elem;
	temp->prox=lista;
	return temp;
}

void distruggilistaric(ListaDiElem lista){
	if(lista!=NULL){
		distruggilistaric(lista->prox);
		free(lista);
	}
}

int verificapresenzaric(ListaDiElem lista, TipoElemento elem){
	if(lista==NULL){
		return 0;
	}
	if(lista->info==elem){
		return 1;
	}
	return verificapresenzaric(lista->prox, elem);
}

void visualizzalistaric(ListaDiElem lista){
	ListaDiElem temp;
	if(lista==NULL){
		printf("--||\n");
	}
	else{
		printf("%d", lista->info);
		if(lista->prox!=NULL){
			printf("-->");
		}
		visualizzalistaric(lista->prox);
	}
}
