#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

int contenuta(structg griglia, char parola[]){
	
	
	structg griglia2;
	for(i=0; i<4; i++){	//copia del boggle originale nel boggle2 (ci servir� per ripristinarlo)
		for(t=0; t<4; t++){
			griglia2.boggle[i][t]=griglia.boggle[i][t];
		}
	}
	int i,t, k;
	for(i=0; i<4; i++){
		for(t=0; t<4; t++){
			if(parola[0]==griglia.boggle[i][t]){	//ricerca casella uguale al primo carattere della stringa
				//verifica la sequenza
				for(k=1; k<=strlen(parola)-1; k++){
					if((i-1>=0) && (t-1>=0) && parola[k]==griglia.boggle[i-1][t-1]){ //verifica con "ricordo" (pr tutti gli if)
						griglia.boggle[i-1][t-1]='\0';
						i--;
						t--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((i-1>=0) parola[k]==griglia.boggle[i-1][t]){
						griglia.boggle[i-1][t]='\0';
						i--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((i-1>=0) && (t+1<=3) && parola[k]==griglia.boggle[i-1][t+1]){
						griglia.boggle[i-1][t+1]='\0';
						i--;
						t++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((t-1>=0) && parola[k]==griglia.boggle[i][t-1]){
						griglia.boggle[i][t-1]='\0';
						t--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((t+1<=3) && parola[k]==griglia.boggle[i][t+1]){
						griglia.boggle[i][t+1]='\0';
						t++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((i+1<=3) && (t-1>=0) && parola[k]==griglia.boggle[i+1][t-1]){
						griglia.boggle[i+1][t-1]='\0';
						i++;
						t--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((i+1<=3) && parola[k]==griglia.boggle[i+1][t]){
						griglia.boggle[i+1][t]='\0';
						i++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					if((i+1<=3) && (t+1<=3) && parola[k]==griglia.boggle[i+1][t+1]){
						griglia.boggle[i+1][t+1]='\0';
						i++;
						t++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else{
						k=strlen(parola)-1;
						for(i=0; i<4; i++){	//se non trova corrispondenze, esce, e il boggle1 viene ripristinato all'originale assegnandogli il boggle2
							for(t=0; t<4; t++){
								griglia.boggle[i][t]=griglia2.boggle[i][t];
							}
						}
					}
				}
			}
		}
	}
	return 0;
}
