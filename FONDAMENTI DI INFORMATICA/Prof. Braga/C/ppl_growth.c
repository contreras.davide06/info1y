#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main() {
    
    float EV, SV, CAGR = 0.0, time_req;
    char risp;
    
    printf("Insert SV: ");
    scanf("%f", &SV);
    
    printf("Insert EV: ");
    scanf("%f", &EV);
    
    printf("Want to know time (T) or CAGR (C)?: ");
    scanf(" %c", &risp);
    
    switch (risp) {
            
        case 't':
            
            printf("Insert CAGR: ");
            scanf("%f", &CAGR);
            
            printf("Time requested in years is: %f\n", time_req = logf(EV/SV)/log1pf(CAGR));
            
            break;
            
        case 'c':
            
            printf("Insert time: ");
            scanf("%f", &time_req);
            
            printf("Cumulative Average Growth Rate is: %.2f%%\n", (CAGR = (pow((EV/SV), (1/time_req))-1))*100);
            
            break;
            
    }
    
    return 0;

}
